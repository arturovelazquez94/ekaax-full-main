
package mx.com.geany.user.customer.controller.impl;


import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import mx.com.geany.user.customer.controller.IUserController;
import mx.com.geany.user.customer.domain.User;
import mx.com.geany.user.customer.helper.ControllerHelper;
import mx.com.geany.user.customer.service.IUserService;



@Component
public class UserControllerImpl implements IUserController {

	@Autowired
	private IUserService userService;

	@Override
	public ResponseEntity<?> find() {
		return ControllerHelper.responseOK(userService.findAll());
	}

	@Override
	public ResponseEntity<?> findById(@PathVariable UUID id) {
		return ControllerHelper.responseOK(userService.findById(id));
	}

	@Override
	public ResponseEntity<?> create(@RequestBody User user) {
		return ControllerHelper.responseCreated(userService.create(user));
	}

}
