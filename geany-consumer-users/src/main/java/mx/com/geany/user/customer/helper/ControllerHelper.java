
package mx.com.geany.user.customer.helper;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ControllerHelper {

	public static <T> ResponseEntity<T> responseOK(T body) {
		return new ResponseEntity<T>(body, HttpStatus.OK);
	}

	public static <T> ResponseEntity<T> responseCreated(T body) {
		return new ResponseEntity<T>(body, HttpStatus.CREATED);
	}
}
