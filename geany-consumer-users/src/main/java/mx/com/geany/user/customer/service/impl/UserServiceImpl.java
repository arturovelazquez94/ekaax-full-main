
package mx.com.geany.user.customer.service.impl;


import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.geany.user.customer.domain.User;
import mx.com.geany.user.customer.repository.UserRepository;
import mx.com.geany.user.customer.service.IUserService;


@Service
public class UserServiceImpl implements IUserService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public User create(User user) {
		return userRepository.save(user);
	}

	@Override
	public List<User> findAll() {
		return userRepository.findAll();
	}

	@Override
	public User findById(UUID id) {
		return userRepository.findById(id).orElseThrow(() -> new RuntimeException("User Not found"));
	}

	@Override
	public void delete(User user) {
		userRepository.delete(user);

	}

}
