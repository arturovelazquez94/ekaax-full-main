
package mx.com.geany.user.customer.controller;


import java.util.UUID;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.com.geany.user.customer.domain.User;



@RestController
@RequestMapping("/users")
public interface IUserController {

	@RequestMapping
	ResponseEntity<?> find();

	@RequestMapping("/{id}")
	ResponseEntity<?> findById(UUID id);

	@PostMapping
	ResponseEntity<?> create(User user);

}
