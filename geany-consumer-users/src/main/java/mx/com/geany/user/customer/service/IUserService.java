
package mx.com.geany.user.customer.service;


import java.util.List;
import java.util.UUID;

import mx.com.geany.user.customer.domain.User;



public interface IUserService {

	User create(User user);

	List<User> findAll();

	User findById(UUID id);

	void delete(User id);

}
