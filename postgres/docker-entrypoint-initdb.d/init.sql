-- -----------------------------------------------------
-- Database Kiwiik
-- -----------------------------------------------------
CREATE EXTENSION dblink;
DO
$do$
BEGIN
   IF EXISTS (SELECT 1 FROM pg_database WHERE datname = 'geany') THEN
      RAISE NOTICE 'Database already exists'; 
   ELSE
      PERFORM dblink_exec('dbname=' || current_database()  -- current db
                        , 'CREATE DATABASE geany');
   END IF;
END
$do$;

\c geany

-- -----------------------------------------------------
-- Extension pgcrypto
-- -----------------------------------------------------
CREATE EXTENSION IF NOT EXISTS "pgcrypto";
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-- -----------------------------------------------------
-- Schema kiwiik_business
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS geany_users CASCADE;
CREATE SCHEMA IF NOT EXISTS geany_users;

-- -----------------------------------------------------
-- Table kiwiik_business.kiwiik_user
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS geany_users.users
  (
    -- Core
    id           UUID         NOT NULL,
    name VARCHAR(60)  NOT NULL,
    last_name           VARCHAR(60)  NOT NULL,
    age    INTEGER	NOT NULL,

    PRIMARY KEY (id)

  );